package com.safebear.tasklist.service;

import com.safebear.tasklist.model.Task;

public interface TaskService {

    Iterable<Task> list();

    Task save(Task task);
}
