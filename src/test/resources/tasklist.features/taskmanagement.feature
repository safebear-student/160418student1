Feature: Task Management

  User Story:
  In order to manage my tasks
  As a user
  I must be able to view, add, archive or update tasks.

  Rules:
  -You must be able to view all tasks
  -They must be paginated
  -You must be able to see the name, due date and status of a task
  -You must be ale to change the status of a task
  -You must be able to archive tasks

  Questions:
  -How many tasks per page for the pagination?
  -Where do you archive to?
  -Is the due date configurable or should it be hard-coded to a certain number of days in the future?

  To Do:
  -Pagination
  -Archiving

  Domain Language:
  -Task = this is made up of a description, a status and a due date.
  -Due Date = this must be in the format dd/MM/YYYY.
  -Status = this is 'completed' or 'not completed'.
  -Task List = this is a list of tasks and can be displayed on the home page.

Background:
  Given the following tasks are created:
    |Configure Jenkins|
    |Set up automation environment|
    |Set up SQL server|

  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
      | task |
      | Create documentation |