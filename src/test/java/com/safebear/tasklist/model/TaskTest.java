package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {

    @Test
    public void creation(){
        LocalDate dueDate = LocalDate.now();
        Task task = new Task(1L, "Configure Jenkins server", dueDate, false);

        // AssertJ
        Assertions.assertThat(task.getId()).isEqualTo(1L);
        Assertions.assertThat(task.getName()).isNotBlank();
        Assertions.assertThat(task.getName()).isEqualTo("Configure Jenkins server");
        Assertions.assertThat(task.getDueDate()).isEqualTo(dueDate);
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);

        // JUnit
        Assert.assertEquals(task.getId(), (Long) 1L);

        // hamcrest
        Assert.assertThat(task.getId(), Matchers.equalTo(1L));
    }
}
