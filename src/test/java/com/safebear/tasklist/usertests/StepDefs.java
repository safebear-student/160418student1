package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class StepDefs {

    //getting URL details
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT = System.getProperty("context");

    // Need to add your browser variable!!!

    WebDriver driver;

    TaskListPage taskListPage;

    @Before
    public void setUp(){

        String browser = System.getProperty("browser");

        switch (browser) {
            case "headless":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                break;
            case "chrome":
                driver = new ChromeDriver();
                break;
            default:
                driver = new ChromeDriver();
                break;
        }

        taskListPage = new TaskListPage(driver);

        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;

        driver.get(url);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(){
        driver.quit();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname){
        taskListPage.addTask(taskname);
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) {
        Assertions.assertThat(taskListPage.checkForTask(taskname)).isTrue();

    }
}
