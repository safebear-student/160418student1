pipeline {

    options {
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }

    agent any

    parameters {

        //tests to run
        //API
        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'API Tests')

        // Cucumber
        string(name: 'cuke', defaultValue: 'RunCukesTestIT', description: 'cucumber tests')
        string(name: 'browser', defaultValue: 'chrome', description: 'browser type')

        //General
        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://18.236.63.138', description: 'application context')

        // Test Environment
        string(name: 'test_hostname', defaultValue: '18.236.63.138', description: 'hostname of test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test environment')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of test environment')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of test environment')
    }
    // Comment

    triggers{
        pollSCM('* * * * *') // This polls the source code repo every minute.
        //  pollSCM('H/5 * * * *') H means 'only run if another job isn't runnning'
        //  pollSCM ('* 0 * * *') // Poll at midnight
    }

    // Pipeline stages
    stages {
        stage('Build with Unit Testing') {
            /* run the mvn package command to ensure build the app and run the unit */
            steps {
                sh 'mvn clean package sonar:sonar   -Dsonar.organization=safebear-student-bitbucket   -Dsonar.host.url=https://sonarcloud.io   -Dsonar.login=f08a36f3598527bf9b20d9b861b57c0aaa5937e3'
            }
            post {
                /* only run if the last step succeeds */
                success {
                    // Print a message to screen that we're archiving
                    echo 'Now Archiving...'
                    // Archive the artifacts so we can build once and then use them later to deploy
                    archiveArtifacts artifacts: '**/target/*.war'
                }
                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }
        stage('Static Analysis') {
            /* run the mvn checkstyle command to run the static analysis.
            Can be done in parallel to the Build and Unit Testing step */
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success {
                    checkstyle canComputeNew: false, defaultEncoding:'', healthy: '', pattern: '', unHealthy: ''
                }
            }
        }

        stage('Deploy to Test'){
//            Deploy to the test environment so we can run our integration and BDD tests
            steps{
                //Deploy using the cargo plugin in the pom.xml maven file
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
            }
        }

        stage('Integration Tests') {
            steps {
                sh "mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"
            }

            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }
        }

        stage('Cucumber Tests') {
            /* run the mvn test command passing the parameter "RunCukesTestIT" */
            steps {
                sh "mvn -Dtest=${cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context} -Dbrowser=${params.browser}"
            }
            post {
                always {
                    publishHTML([
                            allowMissing    : false,
                            alwaysLinkToLastBuild : false,
                            keepAll         : false,
                            reportDir       : 'target/cucumber',
                            reportFiles     : 'index.html',
                            reportName      : 'BDD Report',
                            reportTitles    : ''])
                }
            }
        }
    }
}